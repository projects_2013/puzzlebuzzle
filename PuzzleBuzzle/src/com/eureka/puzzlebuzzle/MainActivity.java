package com.eureka.puzzlebuzzle;

import com.eureka.puzzlebuzzle.preferences.ScorePreferences;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class MainActivity extends Activity implements AnimationListener {

	// private TextView instructions_TextView;
	private Animation animFadein;
	private ImageView animLogo;
	public ScorePreferences preferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		preferences = new ScorePreferences(getApplicationContext());
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_page);

		findViewById(R.id.instructions).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), InstructionsActivity.class);
				startActivity(intent);
			}
		});

		findViewById(R.id.website_link).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("http://www.javatpoint.com/android-tutorial"));
				startActivity(intent);
			}
		});

		// load the animation
		animFadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.right_to_left);

		// set animation listener
		animFadein.setAnimationListener(this);

		// display the next image during 2 seconds
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				animLogo = (ImageView) findViewById(R.id.anim_Logo);
				animLogo.startAnimation(animFadein);
				animLogo.setVisibility(View.INVISIBLE);
			}
		}, 2000);

		findViewById(R.id.play_Button).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), GameLevels.class);
				startActivity(intent);
			}

		});
		findViewById(R.id.quit_Button).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
				preferences.clearScore();
			}

		});

		// instructions_TextView = (TextView)
		// findViewById(R.id.homePageInstuctions);
		// instructions_TextView.setMovementMethod(new
		// ScrollingMovementMethod());

		// String text =
		// "This is the instructions area. Where we can see the how to play the game and all... puzzle buzzle is an app where we can play the game and post advertisement ";
		// instructions_TextView.setText(text);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onAnimationStart(Animation animation) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAnimationEnd(Animation animation) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		//clear the global level score
		preferences.clearScore();
	}
}
