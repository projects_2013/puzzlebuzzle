package com.eureka.puzzlebuzzle.games.medium;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.drawable.StateListDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.eureka.puzzlebuzzle.EndActivity;
import com.eureka.puzzlebuzzle.R;

public class MediumGame4 extends Activity {
	private int time2 = 0;
	private Timer t2;
	private TimerTask task2;
	private TextView tv3;
	private MediaPlayer player = new MediaPlayer();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.medium_game4);

		startTimer();
		AssetFileDescriptor afd = null;
		try {
			afd = getAssets().openFd("song.mp3");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			player.prepare();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		player.start();

		final TextView score = (TextView) findViewById(R.id.score3);

		tv3 = (TextView) findViewById(R.id.timer_m);

		final Button b = (Button) findViewById(R.id.logomb);

		Button b1 = b;
		b1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				EditText e1 = (EditText) findViewById(R.id.logome);
				final ImageView imageview = (ImageView) findViewById(R.id.imageViewMedium);

				final EditText e12 = e1;
				String text1 = e12.getText().toString();

				if (text1.equalsIgnoreCase("amd")) {
					e12.setText(null);
					showSuccessToast();
					score.setText("10");

					StateListDrawable states = new StateListDrawable();
					states.addState(new int[] {}, getResources().getDrawable(R.drawable.lm2));
					imageview.setImageDrawable(states);

					Button b2 = b;
					b2.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub

							EditText e2 = e12;
							e2 = (EditText) findViewById(R.id.logome);
							String text2 = e2.getText().toString();

							if (text2.equalsIgnoreCase("fosters")) {
								e2.setText(null);
								showSuccessToast();
								score.setText("20");

								StateListDrawable states1 = new StateListDrawable();
								states1.addState(new int[] {}, getResources().getDrawable(R.drawable.lm3));
								imageview.setImageDrawable(states1);

								Button b3 = b;
								b3.setOnClickListener(new OnClickListener() {

									@Override
									public void onClick(View v) {
										// TODO Auto-generated method stub

										EditText e3 = e12;
										e3 = (EditText) findViewById(R.id.logome);
										String text3 = e3.getText().toString();

										if (text3.equalsIgnoreCase("hp")) {

											e3.setText(null);
											showSuccessToast();
											score.setText("30");

											StateListDrawable states2 = new StateListDrawable();
											states2.addState(new int[] {}, getResources().getDrawable(R.drawable.lm4));
											imageview.setImageDrawable(states2);

											Button b4 = b;
											b4.setOnClickListener(new OnClickListener() {

												@Override
												public void onClick(View v) {
													// TODO Auto-generated
													// method stub

													EditText e4 = e12;
													e4 = (EditText) findViewById(R.id.logome);
													String text4 = e4.getText().toString();

													if (text4.equalsIgnoreCase("htc")) {

														e4.setText(null);
														showSuccessToast();
														score.setText("40");

														StateListDrawable states3 = new StateListDrawable();
														states3.addState(new int[] {},
																getResources().getDrawable(R.drawable.lm5));
														imageview.setImageDrawable(states3);

														Button b5 = b;
														b5.setOnClickListener(new OnClickListener() {

															@Override
															public void onClick(View v) {
																// TODO
																// Auto-generated
																// method stub

																EditText e5 = e12;
																e5 = (EditText) findViewById(R.id.logome);
																String text5 = e5.getText().toString();

																if (text5.equalsIgnoreCase("manchester united")) {

																	e5.setText(null);
																	showSuccessToast();
																	score.setText("50");

																	StateListDrawable states4 = new StateListDrawable();
																	states4.addState(new int[] {}, getResources()
																			.getDrawable(R.drawable.lm6));
																	imageview.setImageDrawable(states4);

																	Button b6 = b;
																	b6.setOnClickListener(new OnClickListener() {

																		@Override
																		public void onClick(View v) {
																			// TODO
																			// Auto-generated
																			// method
																			// stub

																			EditText e6 = e12;
																			e6 = (EditText) findViewById(R.id.logome);
																			String text6 = e6.getText().toString();

																			if (text6.equalsIgnoreCase("mars")) {
																				e6.setText(null);
																				showSuccessToast();
																				score.setText("60");

																				StateListDrawable states5 = new StateListDrawable();
																				states5.addState(
																						new int[] {},
																						getResources().getDrawable(
																								R.drawable.lm7));
																				imageview.setImageDrawable(states5);

																				Button b7 = b;
																				b7.setOnClickListener(new OnClickListener() {

																					@Override
																					public void onClick(View v) {
																						// TODO
																						// Auto-generated
																						// method
																						// stub

																						EditText e7 = e12;
																						e7 = (EditText) findViewById(R.id.logome);
																						String text7 = e7.getText()
																								.toString();

																						if (text7
																								.equalsIgnoreCase("michelin")) {

																							e7.setText(null);
																							showSuccessToast();
																							score.setText("70");

																							StateListDrawable states6 = new StateListDrawable();
																							states6.addState(
																									new int[] {},
																									getResources()
																											.getDrawable(
																													R.drawable.lm8));
																							imageview
																									.setImageDrawable(states6);

																							Button b8 = b;
																							b8.setOnClickListener(new OnClickListener() {

																								@Override
																								public void onClick(
																										View v) {
																									// TODO
																									// Auto-generated
																									// method
																									// stub

																									EditText e8 = e12;
																									e8 = (EditText) findViewById(R.id.logome);
																									String text8 = e8
																											.getText()
																											.toString();

																									if (text8
																											.equalsIgnoreCase("opel")) {

																										e8.setText(null);
																										showSuccessToast();
																										score.setText("80");

																										StateListDrawable states7 = new StateListDrawable();
																										states7.addState(
																												new int[] {},
																												getResources()
																														.getDrawable(
																																R.drawable.lm9));
																										imageview
																												.setImageDrawable(states7);

																										Button b9 = b;
																										b9.setOnClickListener(new OnClickListener() {

																											@Override
																											public void onClick(
																													View v) {

																												EditText e9 = e12;
																												e9 = (EditText) findViewById(R.id.logome);
																												String text9 = e9
																														.getText()
																														.toString();

																												if (text9
																														.equalsIgnoreCase("peugeot")) {

																													e9.setText(null);
																													showSuccessToast();
																													score.setText("90");

																													StateListDrawable states8 = new StateListDrawable();
																													states8.addState(
																															new int[] {},
																															getResources()
																																	.getDrawable(
																																			R.drawable.lm10));
																													imageview
																															.setImageDrawable(states8);

																													Button b10 = b;
																													b10.setOnClickListener(new OnClickListener() {

																														@Override
																														public void onClick(
																																View v) {
																															EditText e10 = e12;
																															e10 = (EditText) findViewById(R.id.logome);
																															String text10 = e10
																																	.getText()
																																	.toString();

																															if (text10
																																	.equalsIgnoreCase("spyker")) {

																																e10.setText(null);
																																showSuccessToast();
																																score.setText("100");

																																int a = time2;
																																if (a <= 180) {
																																	if (a <= 120) {

																																		if (a <= 60) {

																																			score.setText("200");

																																		}

																																		else {
																																			score.setText("180");
																																		}
																																	} else {
																																		score.setText("160");

																																	}
																																	time2 = 0;
																																}

																																// StateListDrawable
																																// states9
																																// =
																																// new
																																// StateListDrawable();
																																// states9.addState(new
																																// int[]{},getResources().getDrawable(R.drawable.completed));
																																// imageview.setImageDrawable(states9);

																																 Intent
																																 it
																																 =
																																 new
																																 Intent(getApplicationContext(),EndActivity.class);
																																 startActivity(it);

																															} else {
																																showFailureToast();
																															}
																														}
																													});

																												} else {
																													showFailureToast();
																												}
																											}
																										});
																									} else {
																										showFailureToast();
																									}
																								}
																							});
																						} else {
																							showFailureToast();
																						}
																					}
																				});

																			} else {
																				showFailureToast();
																			}
																		}
																	});
																} else {
																	showFailureToast();
																}
															}
														});
													} else {
														showFailureToast();
													}
												}
											});

										} else {
											showFailureToast();
										}

									}
								});
							} else {
								showFailureToast();
							}

						}
					});

				} else {
					showFailureToast();
				}
			}

		});
	}

	private void startTimer() {

		t2 = new Timer();
		task2 = new TimerTask() {

			@Override
			public void run() {
				runOnUiThread(new Runnable() {

					@Override
					public void run() {

						tv3.setText(time2 + "");
						if (time2 < 180) {
							time2 += 1;
						} else {
							showAlert();

						}
					}
				});
			}
		};
		t2.scheduleAtFixedRate(task2, 0, 1000);

	}

	private void showFailureToast() {
		Toast toast = Toast.makeText(getApplicationContext(), "sorry,try again!!", Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
		toast.show();
	}

	private void showSuccessToast() {
		Toast toast = Toast.makeText(getApplicationContext(), "You got it", Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
		toast.show();
	}

	private void showAlert() {

		AlertDialog.Builder builder = new AlertDialog.Builder(MediumGame4.this);
		builder.setTitle("PUM PUM PUM");
		builder.setMessage("OOPS .. TIME OUT ! Wanna try again...???");
		builder.setNegativeButton("try again", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// do nothing

				Intent intent = new Intent(getApplicationContext(), MediumGame4.class);
				startActivity(intent);
			}
		});
		builder.setNeutralButton("exit", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO email sending
			}
		});

		builder.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.medium_game4, menu);
		return true;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		// stop audio when pressed back button
		player.stop();
		player.reset();
	}

	@Override
	protected void onPause() {
		super.onPause();
		player.pause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		player.start();
	}

}
