package com.eureka.puzzlebuzzle.games.medium;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.eureka.puzzlebuzzle.R;

public class MediumGame3 extends Activity {

	private ImageView m3ImageView;
	private TextView countTextView, tv1;
	private String m3_string;
	private EditText m3_TextView;
	private int count = 0;
	private Button button;
	private int time = 0;
	private Timer t;
	private TimerTask task;
	private String countString, timeString;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.medium_game3);

		startTimer();

		m3ImageView = (ImageView) findViewById(R.id.m3_imageview);

		countTextView = (TextView) findViewById(R.id.count);
		tv1 = (TextView) findViewById(R.id.timer);

		button = (Button) findViewById(R.id.m3_button);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				button1Action();
			}
		});

	}

	private void startTimer() {

		t = new Timer();
		task = new TimerTask() {

			@Override
			public void run() {
				runOnUiThread(new Runnable() {

					@Override
					public void run() {

						tv1.setText(time + "");
						if (time < 180) {
							time += 1;
						} else if (time == 180) {
							showAlert5();
						}
					}
				});
			}
		};
		t.scheduleAtFixedRate(task, 0, 1000);

	}

	private void button1Action() {

		countString = countTextView.getText().toString();
		timeString = tv1.getText().toString();
		Integer.parseInt(countString);
		time = Integer.parseInt(timeString);

		m3_TextView = (EditText) findViewById(R.id.m3_editText1);
		m3_string = m3_TextView.getText().toString();
		final String[] m3String = { "blue", "green", "violet", "yellow", "white", "grey", "white", "red", "blue",
				"black", "green", "red", "violet", "green", "blue", "pink", "red", "red", "yellow", "grey", "green" };

		if (m3_string.equalsIgnoreCase(m3String[0]) && count == 0) {
			countTextView.setText("1");
			m3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (m3_string.equalsIgnoreCase(m3String[1]) && count == 1) {
			countTextView.setText("2");
			m3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (m3_string.equalsIgnoreCase(m3String[2]) && count == 2) {
			countTextView.setText("3");
			m3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (m3_string.equalsIgnoreCase(m3String[3]) && count == 3) {
			countTextView.setText("4");
			m3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (m3_string.equalsIgnoreCase(m3String[4]) && count == 4) {
			countTextView.setText("5");
			m3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (m3_string.equalsIgnoreCase(m3String[5]) && count == 5) {
			countTextView.setText("6");
			m3_TextView.setText(null);
			count++;

			showAlert1();

			// show success toast
			Toast toast = Toast.makeText(getApplicationContext(), "Congrats..Moved to next image", Toast.LENGTH_LONG);
			toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
			toast.show();

			// Replace current image with next image to play
			final StateListDrawable states = new StateListDrawable();
			states.addState(new int[] {}, getResources().getDrawable(R.drawable.m2));
			m3ImageView.setImageDrawable(states);

		} else if (m3_string.equalsIgnoreCase(m3String[6]) && count == 6) {
			countTextView.setText("7");
			m3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (m3_string.equalsIgnoreCase(m3String[7]) && count == 7) {
			countTextView.setText("8");
			m3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (m3_string.equalsIgnoreCase(m3String[8]) && count == 8) {
			countTextView.setText("9");
			m3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (m3_string.equalsIgnoreCase(m3String[9]) && count == 9) {
			countTextView.setText("10");
			m3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (m3_string.equalsIgnoreCase(m3String[10]) && count == 10) {
			countTextView.setText("11");
			m3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (m3_string.equalsIgnoreCase(m3String[11]) && count == 11) {
			countTextView.setText("12");
			m3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (m3_string.equalsIgnoreCase(m3String[12]) && count == 12) {
			countTextView.setText("13");
			m3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (m3_string.equalsIgnoreCase(m3String[13]) && count == 13) {
			countTextView.setText("14");
			m3_TextView.setText(null);
			count++;

			// show success toast
			Toast toast = Toast.makeText(getApplicationContext(), "Congrats..Moved to next image", Toast.LENGTH_LONG);
			toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
			toast.show();

			showAlert2();

			// Replace current image with next image to play
			final StateListDrawable states = new StateListDrawable();
			states.addState(new int[] {}, getResources().getDrawable(R.drawable.m3));
			m3ImageView.setImageDrawable(states);

		} else if (m3_string.equalsIgnoreCase(m3String[14]) && count == 14) {
			countTextView.setText("15");
			m3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (m3_string.equalsIgnoreCase(m3String[15]) && count == 15) {
			countTextView.setText("16");
			m3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (m3_string.equalsIgnoreCase(m3String[16]) && count == 16) {
			countTextView.setText("17");
			m3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (m3_string.equalsIgnoreCase(m3String[17]) && count == 17) {
			countTextView.setText("18");
			m3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (m3_string.equalsIgnoreCase(m3String[18]) && count == 18) {
			countTextView.setText("19");
			m3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (m3_string.equalsIgnoreCase(m3String[19]) && count == 19) {
			countTextView.setText("20");
			m3_TextView.setText(null);

			// show success toast
			Toast toast = Toast.makeText(getApplicationContext(), "Congrats...You completed medium level",
					Toast.LENGTH_LONG);
			toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
			toast.show();

			// hide edit text and button
			m3_TextView.setVisibility(View.GONE);
			button.setVisibility(View.GONE);

			// show Congrats image
			final StateListDrawable states = new StateListDrawable();
			states.addState(new int[] {}, getResources().getDrawable(R.drawable.congrats));
			m3ImageView.setImageDrawable(states);

			// hide keyboard
			getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

			// Easy level completed TODO show
			// offer/add
			/*
			 * if (points == 70) { // TODO get points based on time }
			 */

		} else {
			showFailureToast();
		}

	}

	private void showAlert1() {

		AlertDialog.Builder builder = new AlertDialog.Builder(MediumGame3.this);
		builder.setTitle("Special Offer");
		builder.setMessage("You got a 30 % of discount on Galaxy Grand Mobile. Are you want to avail this offer..");
		builder.setNegativeButton("Not now", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// do nothing
			}
		});
		builder.setNeutralButton("Proceed", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO email sending
			}
		});

		builder.show();
	}

	private void showAlert2() {

		AlertDialog.Builder builder = new AlertDialog.Builder(MediumGame3.this);
		builder.setTitle("Special Offer");
		builder.setMessage("You got a 20 % of discount on Dell Laptop. Are you want to avail this offer..");
		builder.setNegativeButton("Not now", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// do nothing
			}
		});
		builder.setNeutralButton("Proceed", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO email sending
			}
		});

		builder.show();
	}

	private void showAlert5() {

		AlertDialog.Builder builder = new AlertDialog.Builder(MediumGame3.this);
		builder.setTitle("Time up... :)");
		builder.setMessage("Are you want to play it again..");
		builder.setNegativeButton("Not now", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				setContentView(R.layout.medium);
			}
		});
		builder.setNeutralButton("Proceed", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = getIntent();
				finish();
				startActivity(intent);
			}
		});

		builder.show();
	}

	private void showSuccessToast() {
		Toast toast = Toast.makeText(getApplicationContext(), "You got it", Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
		toast.show();
	}

	private void showFailureToast() {
		Toast toast = Toast.makeText(getApplicationContext(), "Wrong guess Try again", Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
		toast.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.medium_game3, menu);
		return true;
	}

}
