package com.eureka.puzzlebuzzle.games.easy;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.drawable.StateListDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.eureka.puzzlebuzzle.EndActivity;
import com.eureka.puzzlebuzzle.MainActivity;
import com.eureka.puzzlebuzzle.R;

public class EasyGame4 extends Activity {
	private int time1 = 0;
	private Timer t1;
	private TimerTask task1;
	private TextView tv2;
	private MediaPlayer player = new MediaPlayer();

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// Remove notification bar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		super.onCreate(savedInstanceState);
		setContentView(R.layout.easy_game4);

		startTimer();
		AssetFileDescriptor afd = null;
		try {
			afd = getAssets().openFd("song.mp3");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			player.prepare();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		player.start();

		final TextView score = (TextView) findViewById(R.id.score1);

		tv2 = (TextView) findViewById(R.id.timer_e);

		final Button b = (Button) findViewById(R.id.e3_button);

		Button b2 = b;
		b2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				EditText e1 = (EditText) findViewById(R.id.dataenter);
				final ImageView imageView = (ImageView) findViewById(R.id.logo_Image);

				final EditText e12 = e1;
				String text1 = e12.getText().toString();

				if (text1.equals("redbull")) {
					e12.setText(null);
					showSuccessToast();
					score.setText("10");

					StateListDrawable states = new StateListDrawable();
					states.addState(new int[] {}, getResources().getDrawable(R.drawable.le2));
					imageView.setImageDrawable(states);

					Button b3 = b;
					b3.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							EditText e2 = e12;
							e2 = (EditText) findViewById(R.id.dataenter);
							String text2 = e2.getText().toString();

							if (text2.equalsIgnoreCase("volvo")) {
								e2.setText(null);
								showSuccessToast();
								score.setText("20");

								StateListDrawable states1 = new StateListDrawable();
								states1.addState(new int[] {}, getResources().getDrawable(R.drawable.le3));
								imageView.setImageDrawable(states1);

								Button b4 = b;
								b4.setOnClickListener(new OnClickListener() {

									@Override
									public void onClick(View v) {
										// TODO Auto-generated method stub

										EditText e3 = e12;
										e3 = (EditText) findViewById(R.id.dataenter);
										String text3 = e3.getText().toString();

										if (text3.equalsIgnoreCase("kodak")) {
											e3.setText(null);
											showSuccessToast();
											score.setText("30");

											StateListDrawable states2 = new StateListDrawable();
											states2.addState(new int[] {}, getResources().getDrawable(R.drawable.le4));
											imageView.setImageDrawable(states2);

											Button b5 = b;
											b5.setOnClickListener(new OnClickListener() {

												@Override
												public void onClick(View v) {
													// TODO Auto-generated
													// method stub

													EditText e4 = e12;
													e4 = (EditText) findViewById(R.id.dataenter);
													String text4 = e4.getText().toString();

													if (text4.equalsIgnoreCase("amazon")) {
														e4.setText(null);
														showSuccessToast();
														score.setText("40");

														StateListDrawable states3 = new StateListDrawable();
														states3.addState(new int[] {},
																getResources().getDrawable(R.drawable.le5));
														imageView.setImageDrawable(states3);

														Button b6 = b;
														b6.setOnClickListener(new OnClickListener() {

															@Override
															public void onClick(View v) {
																// TODO
																// Auto-generated
																// method stub

																EditText e5 = e12;
																e5 = (EditText) findViewById(R.id.dataenter);
																String text5 = e5.getText().toString();

																if (text5.equalsIgnoreCase("harley davidson")) {
																	e5.setText(null);
																	showSuccessToast();
																	score.setText("50");

																	StateListDrawable states4 = new StateListDrawable();
																	states4.addState(new int[] {}, getResources()
																			.getDrawable(R.drawable.le6));
																	imageView.setImageDrawable(states4);

																	Button b7 = b;
																	b7.setOnClickListener(new OnClickListener() {

																		@Override
																		public void onClick(View v) {
																			// TODO
																			// Auto-generated
																			// method
																			// stub

																			EditText e6 = e12;
																			e6 = (EditText) findViewById(R.id.dataenter);
																			String text6 = e6.getText().toString();

																			if (text6.equalsIgnoreCase("vodafone")) {
																				e6.setText(null);
																				showSuccessToast();
																				score.setText("60");

																				StateListDrawable states5 = new StateListDrawable();
																				states5.addState(
																						new int[] {},
																						getResources().getDrawable(
																								R.drawable.le7));
																				imageView.setImageDrawable(states5);

																				Button b8 = b;
																				b8.setOnClickListener(new OnClickListener() {

																					@Override
																					public void onClick(View v) {
																						// TODO
																						// //
																						// Auto-generated
																						// //
																						// method
																						// //
																						// stub

																						EditText e7 = e12;
																						e7 = (EditText) findViewById(R.id.dataenter);
																						String text7 = e7.getText()
																								.toString();

																						if (text7
																								.equalsIgnoreCase("kwality walls")) {
																							e7.setText(null);
																							showSuccessToast();
																							score.setText("70");

																							StateListDrawable states6 = new StateListDrawable();
																							states6.addState(
																									new int[] {},
																									getResources()
																											.getDrawable(
																													R.drawable.le8));
																							imageView
																									.setImageDrawable(states6);

																							Button b9 = b;
																							b9.setOnClickListener(new OnClickListener() {

																								@Override
																								public void onClick(
																										View v) {
																									// TODO
																									// Auto-generated
																									// method
																									// stub

																									EditText e8 = e12;
																									e8 = (EditText) findViewById(R.id.dataenter);
																									String text8 = e8
																											.getText()
																											.toString();

																									if (text8
																											.equalsIgnoreCase("mozilla firefox")) {
																										e8.setText(null);
																										showSuccessToast();
																										score.setText("80");

																										StateListDrawable states7 = new StateListDrawable();
																										states7.addState(
																												new int[] {},
																												getResources()
																														.getDrawable(
																																R.drawable.le9));

																										imageView
																												.setImageDrawable(states7);

																										Button b10 = b;
																										b10.setOnClickListener(new OnClickListener() {

																											@Override
																											public void onClick(
																													View v) {
																												// TODO
																												// Auto-generated
																												// method
																												// stub

																												EditText e9 = e12;
																												e9 = (EditText) findViewById(R.id.dataenter);
																												String text9 = e9
																														.getText()
																														.toString();

																												if (text9
																														.equalsIgnoreCase("pepejeans")) {
																													e9.setText(null);
																													showSuccessToast();
																													score.setText("90");

																													StateListDrawable states8 = new StateListDrawable();
																													states8.addState(
																															new int[] {},
																															getResources()
																																	.getDrawable(
																																			R.drawable.le10));

																													imageView
																															.setImageDrawable(states8);

																													Button b11 = b;
																													b11.setOnClickListener(new OnClickListener() {

																														@Override
																														public void onClick(
																																View v) {
																															// TODO
																															// Auto-generated
																															// method
																															// stub

																															EditText e10 = e12;
																															e10 = (EditText) findViewById(R.id.dataenter);
																															String text10 = e10
																																	.getText()
																																	.toString();

																															if (text10
																																	.equalsIgnoreCase("skype")) {
																																e10.setText(null);
																																showSuccessToast();
																																score.setText("100");

																																int a = time1;
																																if (a <= 180) {
																																	if (a <= 120) {

																																		if (a <= 60) {

																																			score.setText("200");

																																		}

																																		else {
																																			score.setText("180");
																																		}
																																	} else {
																																		score.setText("160");

																																	}
																																	time1 = 0;
																																}

																																// StateListDrawable
																																// states9
																																// =
																																// new
																																// StateListDrawable();
																																// states9.addState(
																																// new
																																// int[]
																																// {},
																																// getResources()
																																// .getDrawable(
																																// R.drawable.completed));
																																//
																																// imageView
																																// .setImageDrawable(states9);

																																Intent it = new Intent(
																																		getApplicationContext(),
																																		EndActivity.class);
																																startActivity(it);

																															} else {
																																showFailureToast();
																															}
																														}
																													});
																												} else {
																													showFailureToast();
																												}
																											}
																										});
																									} else {
																										showFailureToast();
																									}

																								}
																							});
																						} else {
																							showFailureToast();
																						}
																					}
																				});

																			} else {
																				showFailureToast();
																			}
																		}
																	});

																} else {
																	showFailureToast();
																}
															}
														});

													} else {
														showFailureToast();
													}

												}
											});

										} else {
											showFailureToast();
										}
									}
								});
							} else {
								showFailureToast();
							}

						}
					});

				} else {
					showFailureToast();
				}

			}
		});
	}

	private void startTimer() {

		t1 = new Timer();
		task1 = new TimerTask() {

			@Override
			public void run() {
				runOnUiThread(new Runnable() {

					@Override
					public void run() {

						tv2.setText(time1 + "");
						if (time1 < 180) {
							time1 += 1;
						} else {
							showAlert();

						}
					}
				});
			}
		};
		t1.scheduleAtFixedRate(task1, 0, 1000);

	}

	private void showFailureToast() {
		Toast toast = Toast.makeText(getApplicationContext(), "sorry,try again!!", Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
		toast.show();
	}

	private void showSuccessToast() {
		Toast toast = Toast.makeText(getApplicationContext(), "You got it", Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
		toast.show();
	}

	private void showAlert() {

		AlertDialog.Builder builder = new AlertDialog.Builder(EasyGame4.this);
		builder.setTitle("PUM PUM PUM");
		builder.setMessage("OOPS .. TIME OUT ! Wanna try again...???");
		builder.setNegativeButton("try again", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// do nothing

				Intent intent = new Intent(getApplicationContext(), MainActivity.class);
				startActivity(intent);
			}
		});
		builder.setNeutralButton("exit", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO email sending
			}
		});

		builder.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.easy_game4, menu);
		return true;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		// stop audio when pressed back button
		player.stop();
		player.reset();
	}

	@Override
	protected void onPause() {
		super.onPause();
		player.pause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		player.start();
	}

}
