package com.eureka.puzzlebuzzle.games.easy;

import name.livitski.games.puzzle.android.GamePlay;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

public class EasyGame1 extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// Remove notification bar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		super.onCreate(savedInstanceState);
		// setContentView(R.layout.easy_game2);
		inVokeEasyGame();
	}

	private void inVokeEasyGame() {
		Intent intent = new Intent(getApplicationContext(), GamePlay.class);
		startActivity(intent);
		
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}
}
