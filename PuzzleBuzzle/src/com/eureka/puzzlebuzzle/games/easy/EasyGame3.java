package com.eureka.puzzlebuzzle.games.easy;

import java.util.Timer;
import java.util.TimerTask;

import net.frakbot.accounts.chooser.AccountChooser;
import android.R.string;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.eureka.puzzlebuzzle.GameLevels;
import com.eureka.puzzlebuzzle.R;
import com.eureka.puzzlebuzzle.asynctask.SendMail;
import com.eureka.puzzlebuzzle.preferences.ScorePreferences;
import com.eureka.puzzlebuzzle.sendmail.GMailSender;

public class EasyGame3 extends Activity {

	private static final int AUTHORIZATION_CODE = 1993;
	private static final int ACCOUNT_CODE = 1601;
	public ScorePreferences preferences;

	private ImageView e3ImageView;
	private TextView tv1;
	private String e3_string;
	private EditText e3_TextView;
	private int count;
	private Button button;
	private int time = 0;
	private Timer t;
	private TimerTask task;
	private String timeString;
	private String accountName;
	private String randomCode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		StrictMode.ThreadPolicy policy = new
				 StrictMode.ThreadPolicy.Builder().permitAll().build();
				        StrictMode.setThreadPolicy(policy);

		preferences = new ScorePreferences(getApplicationContext());
		count = preferences.getScore();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.easy_game3);

		startTimer();

		e3ImageView = (ImageView) findViewById(R.id.e3_imageview);

		tv1 = (TextView) findViewById(R.id.timer);

		button = (Button) findViewById(R.id.e3_button);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				button1Action();
			}
		});

	}

	private void startTimer() {

		t = new Timer();
		task = new TimerTask() {

			@Override
			public void run() {
				runOnUiThread(new Runnable() {

					@Override
					public void run() {

						tv1.setText(time + "");
						if (time < 180) {
							time += 1;
						} else if (time == 180) {
							showTimeUpAlert();
						}
					}
				});
			}
		};
		t.scheduleAtFixedRate(task, 0, 1000);

	}

	@SuppressLint("NewApi")
	private void button1Action() {

		sendEmail();

		invalidateOptionsMenu();

		timeString = tv1.getText().toString();
		time = Integer.parseInt(timeString);

		e3_TextView = (EditText) findViewById(R.id.e3_editText1);
		e3_string = e3_TextView.getText().toString();
		final String[] e3String = { "red", "violet", "blue", "blue", "black", "white", "red", "pink", "brown",
				"yellow", "blue", "black", "green", "black", "violet", "red", "yellow", "red", "green", "blue" };

		if (e3_string.equalsIgnoreCase(e3String[0]) && count == 200) {
			e3_TextView.setText(null);
			count++;
			invalidateOptionsMenu();
			showSuccessToast();
		} else if (e3_string.equalsIgnoreCase(e3String[1]) && count == 201) {
			e3_TextView.setText(null);
			invalidateOptionsMenu();
			count++;
			showSuccessToast();
		} else if (e3_string.equalsIgnoreCase(e3String[2]) && count == 202) {
			e3_TextView.setText(null);
			count++;
			invalidateOptionsMenu();
			showSuccessToast();
		} else if (e3_string.equalsIgnoreCase(e3String[3]) && count == 203) {
			e3_TextView.setText(null);
			count++;
			invalidateOptionsMenu();
			showSuccessToast();
		} else if (e3_string.equalsIgnoreCase(e3String[4]) && count == 204) {
			e3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (e3_string.equalsIgnoreCase(e3String[5]) && count == 205) {
			e3_TextView.setText(null);
			count++;
			invalidateOptionsMenu();

			showAlertAdd1();

			// show success toast
			Toast toast = Toast.makeText(getApplicationContext(), "Congrats..Moved to next image", Toast.LENGTH_LONG);
			toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
			toast.show();

			// Replace current image with next image to play
			final StateListDrawable states = new StateListDrawable();
			states.addState(new int[] {}, getResources().getDrawable(R.drawable.e2));
			e3ImageView.setImageDrawable(states);

		} else if (e3_string.equalsIgnoreCase(e3String[6]) && count == 206) {
			e3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (e3_string.equalsIgnoreCase(e3String[7]) && count == 207) {
			e3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (e3_string.equalsIgnoreCase(e3String[8]) && count == 208) {
			e3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (e3_string.equalsIgnoreCase(e3String[9]) && count == 209) {
			e3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (e3_string.equalsIgnoreCase(e3String[10]) && count == 210) {
			e3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (e3_string.equalsIgnoreCase(e3String[11]) && count == 211) {
			e3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (e3_string.equalsIgnoreCase(e3String[12]) && count == 212) {
			e3_TextView.setText(null);
			count++;

			// show success toast
			Toast toast = Toast.makeText(getApplicationContext(), "Congrats..Moved to next image", Toast.LENGTH_LONG);
			toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
			toast.show();

			showAlertAdd2();

			// Replace current image with next image to play
			final StateListDrawable states = new StateListDrawable();
			states.addState(new int[] {}, getResources().getDrawable(R.drawable.e3));
			e3ImageView.setImageDrawable(states);

		} else if (e3_string.equalsIgnoreCase(e3String[13]) && count == 213) {
			e3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (e3_string.equalsIgnoreCase(e3String[14]) && count == 214) {
			e3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (e3_string.equalsIgnoreCase(e3String[15]) && count == 215) {
			e3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (e3_string.equalsIgnoreCase(e3String[16]) && count == 216) {
			e3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (e3_string.equalsIgnoreCase(e3String[17]) && count == 217) {
			e3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (e3_string.equalsIgnoreCase(e3String[18]) && count == 218) {
			e3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (e3_string.equalsIgnoreCase(e3String[19]) && count == 219) {
			e3_TextView.setText(null);

			// show success toast
			Toast toast = Toast.makeText(getApplicationContext(), "Congrats...You completed easy level",
					Toast.LENGTH_LONG);
			toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
			toast.show();

			// hide edit text and button
			e3_TextView.setVisibility(View.GONE);
			button.setVisibility(View.GONE);

			// show Congrats image
			final StateListDrawable states = new StateListDrawable();
			states.addState(new int[] {}, getResources().getDrawable(R.drawable.congrats));
			e3ImageView.setImageDrawable(states);

			// hide keyboard
			getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

			// Easy level completed TODO show
			// offer/add
			/*
			 * if (points == 70) { // TODO get points based on time }
			 */

		} else {
			showFailureToast();
		}

	}

	private void showAlertAdd1() {

		AlertDialog.Builder builder = new AlertDialog.Builder(EasyGame3.this);
		builder.setTitle("Special Offer");
		builder.setMessage("You got a 30 % of discount on Galaxy Grand Mobile. Are you want to avail this offer..");
		builder.setNegativeButton("Not now", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// do nothing
			}
		});
		builder.setNeutralButton("Proceed", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// send mail
				sendEmail();
			}
		});

		builder.show();
	}

	private void showAlertAdd2() {

		AlertDialog.Builder builder = new AlertDialog.Builder(EasyGame3.this);
		builder.setTitle("Special Offer");
		builder.setMessage("You got a 20 % of discount on Dell Laptop. Are you want to avail this offer..");
		builder.setNegativeButton("Not now", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// do nothing
			}
		});
		builder.setNeutralButton("Proceed", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// send mail
				sendEmail();
			}

		});

		builder.show();
	}

	private void showTimeUpAlert() {

		AlertDialog.Builder builder = new AlertDialog.Builder(EasyGame3.this);
		builder.setTitle("Time up... :)");
		builder.setMessage("Are you want to play it again..");
		builder.setNegativeButton("Not now", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				setContentView(R.layout.easy);
			}
		});
		builder.setNeutralButton("Proceed", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = getIntent();
				finish();
				startActivity(intent);
			}
		});

		builder.show();
	}

	private void sendEmail() {
		Log.i("Send email", "");
		Intent intent = AccountChooser.newChooseAccountIntent(null, null, new String[] { "com.google" }, true, null,
				null, null, null, getApplicationContext());
		startActivityForResult(intent, ACCOUNT_CODE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_OK) {
			if (requestCode == AUTHORIZATION_CODE) {

			} else if (requestCode == ACCOUNT_CODE) {
				accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
				// send mail here
				sendMail(accountName);
				Intent i = new Intent(this, GameLevels.class);
				// this.startActivity(i);

			}
		} else if (resultCode == RESULT_CANCELED) {
			Intent i = new Intent(this, EasyGame3.class);
			this.startActivity(i);
		}
	}

	private void sendMail(String accountName) {/*
							 * 
							 * randomCode =
							 * RandomStringUtils.randomAlphanumeric(8);
							 * 
							 * String[] TO = { accountName }; String[] CC = {
							 * "shylendramadda@gmail.com" }; Intent emailIntent
							 * = new Intent(Intent.ACTION_SEND);
							 * emailIntent.setData(Uri.parse("mailto:"));
							 * emailIntent.setType("text/plain");
							 * 
							 * emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
							 * emailIntent.putExtra(Intent.EXTRA_CC, CC);
							 * emailIntent.putExtra(Intent.EXTRA_SUBJECT,
							 * "Offer from Puzzle Buzzle game");
							 * emailIntent.putExtra(Intent.EXTRA_TEXT,
							 * "Hai this mail is from Puzzle buzzle. You got a special offer on....etc"
							 * + "Your secret code is	  " + randomCode);
							 * 
							 * try {
							 * startActivity(Intent.createChooser(emailIntent,
							 * "Send mail...")); finish();
							 * Log.i("Finished sending email...", ""); } catch
							 * (android.content.ActivityNotFoundException ex) {
							 * Toast.makeText(EasyGame3.this,
							 * "There is no email client installed.",
							 * Toast.LENGTH_SHORT).show(); }
							 */
		SendMail mail = new SendMail(accountName,getApplicationContext());
		mail.execute();

	}

	private void showSuccessToast() {
		Toast toast = Toast.makeText(getApplicationContext(), "You got it", Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
		toast.show();
	}

	private void showFailureToast() {
		Toast toast = Toast.makeText(getApplicationContext(), "Wrong guess Try again", Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
		toast.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.easy_game3, menu);
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem score = (MenuItem) menu.findItem(R.id.scoreInActionBar);
		score.setTitle(String.valueOf(count));
		return true;
	}

}
