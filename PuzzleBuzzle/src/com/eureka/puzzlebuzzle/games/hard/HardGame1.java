package com.eureka.puzzlebuzzle.games.hard;

import name.livitski.games.puzzle.android.HardGamePlay;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

public class HardGame1 extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// Remove notification bar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		super.onCreate(savedInstanceState);
		// setContentView(R.layout.hard_game1);
		inVokeHardGame();
	}

	private void inVokeHardGame() {
		Intent intent = new Intent(getApplicationContext(), HardGamePlay.class);
		startActivity(intent);

	}

}
