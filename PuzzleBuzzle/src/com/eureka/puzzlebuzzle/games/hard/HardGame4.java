package com.eureka.puzzlebuzzle.games.hard;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.drawable.StateListDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.eureka.puzzlebuzzle.EndActivity;
import com.eureka.puzzlebuzzle.MainActivity;
import com.eureka.puzzlebuzzle.R;

public class HardGame4 extends Activity {
	private int time3 = 0;
	private Timer t3;
	private TimerTask task3;
	private TextView tv4;
	private MediaPlayer player = new MediaPlayer();

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// Remove notification bar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		super.onCreate(savedInstanceState);
		setContentView(R.layout.hard_game4);

		startTimer();
		playAudio();

		final TextView score = (TextView) findViewById(R.id.score4);

		tv4 = (TextView) findViewById(R.id.timer_h);

		final Button b = (Button) findViewById(R.id.logohb);

		Button b1 = b;
		b1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				EditText e1 = (EditText) findViewById(R.id.logohe);
				final ImageView imageview = (ImageView) findViewById(R.id.logoivh);

				final EditText e12 = e1;
				String text1 = e12.getText().toString();

				if (text1.equalsIgnoreCase("bristol")) {
					e12.setText(null);
					showSuccessToast();
					score.setText("10");

					StateListDrawable states = new StateListDrawable();
					states.addState(new int[] {}, getResources().getDrawable(R.drawable.lh2));
					imageview.setImageDrawable(states);

					Button b2 = b;
					b2.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub

							EditText e2 = e12;
							e2 = (EditText) findViewById(R.id.logohe);
							String text2 = e2.getText().toString();

							if (text2.equalsIgnoreCase("citroen")) {
								e2.setText(null);
								showSuccessToast();
								score.setText("20");

								StateListDrawable states = new StateListDrawable();
								states.addState(new int[] {}, getResources().getDrawable(R.drawable.lh3));
								imageview.setImageDrawable(states);

								Button b3 = b;
								b3.setOnClickListener(new OnClickListener() {

									@Override
									public void onClick(View v) {
										// TODO Auto-generated method stub

										EditText e3 = e12;
										e3 = (EditText) findViewById(R.id.logohe);
										String text3 = e3.getText().toString();

										if (text3.equalsIgnoreCase("dacia")) {
											e3.setText(null);
											showSuccessToast();
											score.setText("30");

											StateListDrawable states = new StateListDrawable();
											states.addState(new int[] {}, getResources().getDrawable(R.drawable.lh4));
											imageview.setImageDrawable(states);

											Button b4 = b;
											b4.setOnClickListener(new OnClickListener() {

												@Override
												public void onClick(View v) {
													// TODO Auto-generated
													// method stub

													EditText e4 = e12;
													e4 = (EditText) findViewById(R.id.logohe);
													String text4 = e4.getText().toString();

													if (text4.equalsIgnoreCase("debian")) {
														e4.setText(null);
														showSuccessToast();
														score.setText("40");

														StateListDrawable states = new StateListDrawable();
														states.addState(new int[] {},
																getResources().getDrawable(R.drawable.lh5));
														imageview.setImageDrawable(states);

														Button b5 = b;
														b5.setOnClickListener(new OnClickListener() {

															@Override
															public void onClick(View v) {
																// TODO
																// Auto-generated
																// method stub

																EditText e5 = e12;
																e5 = (EditText) findViewById(R.id.logohe);
																String text5 = e5.getText().toString();

																if (text5.equalsIgnoreCase("dunhill")) {
																	e5.setText(null);
																	showSuccessToast();
																	score.setText("50");

																	StateListDrawable states = new StateListDrawable();
																	states.addState(new int[] {}, getResources()
																			.getDrawable(R.drawable.lh6));
																	imageview.setImageDrawable(states);

																	Button b6 = b;
																	b6.setOnClickListener(new OnClickListener() {

																		@Override
																		public void onClick(View v) {
																			// TODO
																			// Auto-generated
																			// method
																			// stub

																			EditText e6 = e12;
																			e6 = (EditText) findViewById(R.id.logohe);
																			String text6 = e6.getText().toString();

																			if (text6.equalsIgnoreCase("gimp")) {
																				e6.setText(null);
																				showSuccessToast();
																				score.setText("60");

																				StateListDrawable states = new StateListDrawable();
																				states.addState(
																						new int[] {},
																						getResources().getDrawable(
																								R.drawable.lh7));
																				imageview.setImageDrawable(states);

																				Button b7 = b;
																				b7.setOnClickListener(new OnClickListener() {

																					@Override
																					public void onClick(View v) {
																						// TODO
																						// Auto-generated
																						// method
																						// stub

																						EditText e7 = e12;
																						e7 = (EditText) findViewById(R.id.logohe);
																						String text7 = e7.getText()
																								.toString();

																						if (text7
																								.equalsIgnoreCase("groupon")) {
																							e7.setText(null);
																							showSuccessToast();
																							score.setText("70");

																							StateListDrawable states = new StateListDrawable();
																							states.addState(
																									new int[] {},
																									getResources()
																											.getDrawable(
																													R.drawable.lh8));
																							imageview
																									.setImageDrawable(states);

																							Button b8 = b;
																							b8.setOnClickListener(new OnClickListener() {

																								@Override
																								public void onClick(
																										View v) {
																									// TODO
																									// Auto-generated
																									// method
																									// stub

																									EditText e8 = e12;
																									e8 = (EditText) findViewById(R.id.logohe);
																									String text8 = e8
																											.getText()
																											.toString();

																									if (text8
																											.equalsIgnoreCase("lastfm")) {
																										e8.setText(null);
																										showSuccessToast();
																										score.setText("80");

																										StateListDrawable states = new StateListDrawable();
																										states.addState(
																												new int[] {},
																												getResources()
																														.getDrawable(
																																R.drawable.lh9));
																										imageview
																												.setImageDrawable(states);

																										Button b9 = b;
																										b9.setOnClickListener(new OnClickListener() {

																											@Override
																											public void onClick(
																													View v) {
																												// TODO
																												// Auto-generated
																												// method
																												// stub

																												EditText e9 = e12;
																												e9 = (EditText) findViewById(R.id.logohe);
																												String text9 = e9
																														.getText()
																														.toString();

																												if (text9
																														.equalsIgnoreCase("loreal")) {
																													e9.setText(null);
																													showSuccessToast();
																													score.setText("90");

																													StateListDrawable states = new StateListDrawable();
																													states.addState(
																															new int[] {},
																															getResources()
																																	.getDrawable(
																																			R.drawable.lh10));
																													imageview
																															.setImageDrawable(states);

																													Button b10 = b;
																													b10.setOnClickListener(new OnClickListener() {

																														@Override
																														public void onClick(
																																View v) {
																															// TODO
																															// Auto-generated
																															// method
																															// stub

																															EditText e10 = e12;
																															e10 = (EditText) findViewById(R.id.logohe);
																															String text10 = e10
																																	.getText()
																																	.toString();

																															if (text10
																																	.equalsIgnoreCase("symbian")) {
																																e10.setText(null);
																																showSuccessToast();
																																score.setText("100");
																																int a = time3;
																																if (a <= 180) {
																																	if (a <= 120) {

																																		if (a <= 60) {

																																			score.setText("200");

																																		}

																																		else {
																																			score.setText("180");
																																		}
																																	} else {
																																		score.setText("160");

																																	}
																																	time3 = 0;
																																}

																																// StateListDrawable
																																// states
																																// =
																																// new
																																// StateListDrawable();
																																// states.addState(new
																																// int[]
																																// {},
																																// getResources().getDrawable(R.drawable.completed));
																																// imageview.setImageDrawable(states);

																																 Intent
																																 it
																																 =
																																 new
																																 Intent(getApplicationContext(),EndActivity.class);
																																 startActivity(it);
																															}
																														}
																													});

																												} else {
																													showFailureToast();
																												}
																											}
																										});

																									} else {
																										showFailureToast();
																									}
																								}
																							});

																						} else {
																							showFailureToast();
																						}
																					}
																				});
																			} else {
																				showFailureToast();
																			}
																		}
																	});

																} else {
																	showFailureToast();
																}

															}
														});

													} else {
														showFailureToast();
													}

												}
											});
										} else {
											showFailureToast();
										}
									}
								});

							} else {
								showFailureToast();
							}

						}
					});

				} else {
					showFailureToast();
				}

			}
		});

	}

	private void playAudio() {
		AssetFileDescriptor afd = null;
		try {
			afd = getAssets().openFd("song.mp3");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			player.prepare();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		player.start();
	}

	private void startTimer() {

		t3 = new Timer();
		task3 = new TimerTask() {

			@Override
			public void run() {
				runOnUiThread(new Runnable() {

					@Override
					public void run() {

						tv4.setText(time3 + "");
						if (time3 < 180) {
							time3 += 1;
						} else if (time3 == 180) {
							showAlert();

						}
					}
				});
			}
		};
		t3.scheduleAtFixedRate(task3, 0, 1000);

	}

	private void showFailureToast() {
		Toast toast = Toast.makeText(getApplicationContext(), "sorry,try again!!", Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
		toast.show();
	}

	private void showSuccessToast() {
		Toast toast = Toast.makeText(getApplicationContext(), "You got it", Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
		toast.show();
	}

	private void showAlert() {

		AlertDialog.Builder builder = new AlertDialog.Builder(HardGame4.this);
		builder.setTitle("PUM PUM PUM");
		builder.setMessage("OOPS .. TIME OUT ! Wanna try again...???");
		builder.setNegativeButton("try again", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = new Intent(getApplicationContext(), MainActivity.class);
				startActivity(intent);
			}
		});
		builder.setNeutralButton("exit", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO email sending
			}
		});

		builder.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.hard_game4, menu);
		return true;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		// stop audio when pressed back button
		if (player.isPlaying()) {
			player.stop();
			player.reset();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		player.pause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		player.start();
	}

}
