package com.eureka.puzzlebuzzle.games.hard;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.eureka.puzzlebuzzle.R;

public class HardGame3 extends Activity {

	private ImageView h3ImageView;
	private TextView countTextView, tv1;
	private String h3_string;
	private EditText h3_TextView;
	private int count = 0;
	private Button button;
	private int time = 0;
	private Timer t;
	private TimerTask task;
	private String countString, timeString;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.hard_game3);

		startTimer();

		h3ImageView = (ImageView) findViewById(R.id.h3_imageview);

		countTextView = (TextView) findViewById(R.id.count);
		tv1 = (TextView) findViewById(R.id.timer);

		button = (Button) findViewById(R.id.h3_button);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				button1Action();
			}
		});

	}

	private void startTimer() {

		t = new Timer();
		task = new TimerTask() {

			@Override
			public void run() {
				runOnUiThread(new Runnable() {

					@Override
					public void run() {

						tv1.setText(time + "");
						if (time < 180) {
							time += 1;
						} else if (time == 180) {
							showAlert5();
						}
					}
				});
			}
		};
		t.scheduleAtFixedRate(task, 0, 1000);

	}

	private void button1Action() {

		countString = countTextView.getText().toString();
		timeString = tv1.getText().toString();
		Integer.parseInt(countString);
		time = Integer.parseInt(timeString);

		h3_TextView = (EditText) findViewById(R.id.h3_editText1);
		h3_string = h3_TextView.getText().toString();
		final String[] h3String = { "white", "green", "red", "violet", "green", "yellow", "blue", "blue", "yellow",
				"violet", "white", "red", "green", "red", "pink", "blue", "black", "yellow", "white", "green",
				"violet", "red", "green", "pink", "yellow", "white", "blue", "yellow", "blue", "orange", "white",
				"red", "green", "violet", "yellow", };

		if (h3_string.equalsIgnoreCase(h3String[0]) && count == 0) {
			countTextView.setText("1");
			h3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (h3_string.equalsIgnoreCase(h3String[1]) && count == 1) {
			countTextView.setText("2");
			h3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (h3_string.equalsIgnoreCase(h3String[2]) && count == 2) {
			countTextView.setText("3");
			h3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (h3_string.equalsIgnoreCase(h3String[3]) && count == 3) {
			countTextView.setText("4");
			h3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (h3_string.equalsIgnoreCase(h3String[4]) && count == 4) {
			countTextView.setText("5");
			h3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (h3_string.equalsIgnoreCase(h3String[5]) && count == 5) {
			countTextView.setText("6");
			h3_TextView.setText(null);
			count++;
		} else if (h3_string.equalsIgnoreCase(h3String[6]) && count == 6) {
			countTextView.setText("7");
			h3_TextView.setText(null);
			count++;
		} else if (h3_string.equalsIgnoreCase(h3String[7]) && count == 7) {
			countTextView.setText("8");
			h3_TextView.setText(null);
			count++;
		} else if (h3_string.equalsIgnoreCase(h3String[8]) && count == 8) {
			countTextView.setText("9");
			h3_TextView.setText(null);
			count++;
		} else if (h3_string.equalsIgnoreCase(h3String[9]) && count == 9) {
			countTextView.setText("10");
			h3_TextView.setText(null);
			count++;
		} else if (h3_string.equalsIgnoreCase(h3String[10]) && count == 10) {
			countTextView.setText("11");
			h3_TextView.setText(null);
			count++;
		} else if (h3_string.equalsIgnoreCase(h3String[11]) && count == 11) {
			countTextView.setText("12");
			h3_TextView.setText(null);
			count++;
		} else if (h3_string.equalsIgnoreCase(h3String[12]) && count == 12) {
			countTextView.setText("13");
			h3_TextView.setText(null);
			count++;
		} else if (h3_string.equalsIgnoreCase(h3String[13]) && count == 13) {
			countTextView.setText("14");
			h3_TextView.setText(null);
			count++;

			showAlert1();

			Toast toast = Toast.makeText(getApplicationContext(), "Congrats..Moved to next image", Toast.LENGTH_LONG);
			toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
			toast.show();

			// Replace current image with next image to play
			final StateListDrawable states = new StateListDrawable();
			states.addState(new int[] {}, getResources().getDrawable(R.drawable.h2));
			h3ImageView.setImageDrawable(states);

		} else if (h3_string.equalsIgnoreCase(h3String[14]) && count == 14) {
			countTextView.setText("15");
			h3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (h3_string.equalsIgnoreCase(h3String[15]) && count == 15) {
			countTextView.setText("16");
			h3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (h3_string.equalsIgnoreCase(h3String[14]) && count == 16) {
			countTextView.setText("17");
			h3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (h3_string.equalsIgnoreCase(h3String[15]) && count == 17) {
			countTextView.setText("18");
			h3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (h3_string.equalsIgnoreCase(h3String[16]) && count == 18) {
			countTextView.setText("19");
			h3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (h3_string.equalsIgnoreCase(h3String[17]) && count == 19) {
			countTextView.setText("20");
			h3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (h3_string.equalsIgnoreCase(h3String[18]) && count == 20) {
			countTextView.setText("21");
			h3_TextView.setText(null);
			count++;
		} else if (h3_string.equalsIgnoreCase(h3String[19]) && count == 21) {
			countTextView.setText("22");
			h3_TextView.setText(null);
			count++;
		} else if (h3_string.equalsIgnoreCase(h3String[20]) && count == 22) {
			countTextView.setText("23");
			h3_TextView.setText(null);
			count++;
		} else if (h3_string.equalsIgnoreCase(h3String[21]) && count == 23) {
			countTextView.setText("24");
			h3_TextView.setText(null);
			count++;
		} else if (h3_string.equalsIgnoreCase(h3String[22]) && count == 24) {
			countTextView.setText("25");
			h3_TextView.setText(null);
			count++;
		} else if (h3_string.equalsIgnoreCase(h3String[23]) && count == 25) {
			countTextView.setText("26");
			h3_TextView.setText(null);
			count++;

			// show success toast
			Toast toast = Toast.makeText(getApplicationContext(), "Congrats..Moved to next image", Toast.LENGTH_LONG);
			toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
			toast.show();

			showAlert2();

			// Replace current image with next image to play
			final StateListDrawable states = new StateListDrawable();
			states.addState(new int[] {}, getResources().getDrawable(R.drawable.h3));
			h3ImageView.setImageDrawable(states);

		} else if (h3_string.equalsIgnoreCase(h3String[24]) && count == 26) {
			countTextView.setText("27");
			h3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (h3_string.equalsIgnoreCase(h3String[25]) && count == 27) {
			countTextView.setText("28");
			h3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (h3_string.equalsIgnoreCase(h3String[26]) && count == 28) {
			countTextView.setText("29");
			h3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (h3_string.equalsIgnoreCase(h3String[27]) && count == 29) {
			countTextView.setText("30");
			h3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (h3_string.equalsIgnoreCase(h3String[28]) && count == 30) {
			countTextView.setText("31");
			h3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (h3_string.equalsIgnoreCase(h3String[29]) && count == 31) {
			countTextView.setText("32");
			h3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (h3_string.equalsIgnoreCase(h3String[30]) && count == 32) {
			countTextView.setText("33");
			h3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (h3_string.equalsIgnoreCase(h3String[31]) && count == 33) {
			countTextView.setText("34");
			h3_TextView.setText(null);
			count++;
			showSuccessToast();
		} else if (h3_string.equalsIgnoreCase(h3String[19]) && count == 34) {
			countTextView.setText("35");
			h3_TextView.setText(null);

			// show success toast
			Toast toast = Toast.makeText(getApplicationContext(), "Congrats...You completed hard level",
					Toast.LENGTH_LONG);
			toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
			toast.show();

			// hide edit text and button
			h3_TextView.setVisibility(View.GONE);
			button.setVisibility(View.GONE);

			// show Congrats image
			final StateListDrawable states = new StateListDrawable();
			states.addState(new int[] {}, getResources().getDrawable(R.drawable.congrats));
			h3ImageView.setImageDrawable(states);

			// hide keyboard
			getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

			// Easy level completed TODO show
			// offer/add
			/*
			 * if (points == 70) { // TODO get points based on time }
			 */

		} else {
			showFailureToast();
		}

	}

	private void showAlert1() {

		AlertDialog.Builder builder = new AlertDialog.Builder(HardGame3.this);
		builder.setTitle("Special Offer");
		builder.setMessage("You got a 30 % of discount on Galaxy Grand Mobile. Are you want to avail this offer..");
		builder.setNegativeButton("Not now", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// do nothing
			}
		});
		builder.setNeutralButton("Proceed", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO email sending
			}
		});

		builder.show();
	}

	private void showAlert2() {

		AlertDialog.Builder builder = new AlertDialog.Builder(HardGame3.this);
		builder.setTitle("Special Offer");
		builder.setMessage("You got a 20 % of discount on Dell Laptop. Are you want to avail this offer..");
		builder.setNegativeButton("Not now", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// do nothing
			}
		});
		builder.setNeutralButton("Proceed", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO email sending
			}
		});

		builder.show();
	}

	private void showAlert5() {

		AlertDialog.Builder builder = new AlertDialog.Builder(HardGame3.this);
		builder.setTitle("Time up... :)");
		builder.setMessage("Are you want to play it again..");
		builder.setNegativeButton("Not now", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				setContentView(R.layout.hard);
			}
		});
		builder.setNeutralButton("Proceed", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = getIntent();
				finish();
				startActivity(intent);
			}
		});

		builder.show();
	}

	private void showSuccessToast() {
		Toast toast = Toast.makeText(getApplicationContext(), "You got it", Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
		toast.show();
	}

	private void showFailureToast() {
		Toast toast = Toast.makeText(getApplicationContext(), "Wrong guess Try again", Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
		toast.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.hard_game3, menu);
		return true;
	}

}
