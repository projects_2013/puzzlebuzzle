package com.eureka.puzzlebuzzle.games.hard;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.drawable.StateListDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.eureka.puzzlebuzzle.EndActivity;
import com.eureka.puzzlebuzzle.R;

public class HardGame2 extends Activity {
	private int time = 0;
	private Timer t;
	private TimerTask task;
	private TextView tv1;
	private MediaPlayer player = new MediaPlayer();

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// Remove notification bar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		super.onCreate(savedInstanceState);
		setContentView(R.layout.hard_game2);

		startTimer();
		playAudio();

		final TextView scorem = (TextView) findViewById(R.id.findh_s);
		tv1 = (TextView) findViewById(R.id.findh_t);

		final Button b = (Button) findViewById(R.id.findh_b);

		Button b1 = b;
		b1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				EditText e1 = (EditText) findViewById(R.id.findh_e);
				final ImageView imageView = (ImageView) findViewById(R.id.findh_i);

				final EditText e12 = e1;
				String text1 = e12.getText().toString();

				if (text1.equals("bark")) {

					e12.setText(null);
					showSuccessToast();
					scorem.setText("20");

					StateListDrawable states = new StateListDrawable();
					states.addState(new int[] {}, getResources().getDrawable(R.drawable.click_h));
					imageView.setImageDrawable(states);
					Button b2 = b;
					b2.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							final EditText e2 = e12;
							String text2 = e2.getText().toString();
							if (text2.equals("click")) {
								e2.setText(null);
								showSuccessToast();
								scorem.setText("40");

								StateListDrawable states1 = new StateListDrawable();
								states1.addState(new int[] {}, getResources().getDrawable(R.drawable.criminal_h));
								imageView.setImageDrawable(states1);

								Button b3 = b;
								b3.setOnClickListener(new OnClickListener() {

									@Override
									public void onClick(View arg0) {
										// TODO Auto-generated method stub
										final EditText e3 = e12;
										String text3 = e3.getText().toString();
										if (text3.equals("criminal")) {
											e3.setText(null);
											showSuccessToast();
											scorem.setText("60");

											StateListDrawable states2 = new StateListDrawable();
											states2.addState(new int[] {},
													getResources().getDrawable(R.drawable.death_h));
											imageView.setImageDrawable(states2);
											Button b4 = b;
											b4.setOnClickListener(new OnClickListener() {

												@Override
												public void onClick(View arg0) {
													// TODO Auto-generated
													// method stub
													final EditText e4 = e12;
													String text4 = e4.getText().toString();
													if (text4.equals("death")) {
														e4.setText(null);
														showSuccessToast();
														scorem.setText("80");

														StateListDrawable states3 = new StateListDrawable();
														states3.addState(new int[] {},
																getResources().getDrawable(R.drawable.trekking_h));
														imageView.setImageDrawable(states3);
														Button b5 = b;
														b5.setOnClickListener(new OnClickListener() {

															@Override
															public void onClick(View v) {
																// TODO
																// Auto-generated
																// method stub
																final EditText e5 = e12;
																String text5 = e5.getText().toString();
																if (text5.equals("trekking")) {
																	scorem.setText("100");

																	int a = time;
																	if (a <= 180) {
																		if (a <= 120) {

																			if (a <= 60) {

																				scorem.setText("200");

																			}

																			else {
																				scorem.setText("180");
																			}
																		} else {
																			scorem.setText("160");

																		}
																		time = 0;
																	}
																	 Intent
																	 intent =
																	 new
																	 Intent(getApplicationContext(),
																	 EndActivity.class);
																	 startActivity(intent);

																} else {
																	showFailureToast();
																}
															}
														});

													} else {
														showFailureToast();
													}

												}
											});

										} else {
											showFailureToast();
										}

									}
								});
							} else {
								showFailureToast();
							}

						}
					});

				} else {
					showFailureToast();
				}
			}
		});
	}

	private void playAudio() {
		AssetFileDescriptor afd = null;
		try {
			afd = getAssets().openFd("song.mp3");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			player.prepare();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		player.start();
	}

	private void startTimer() {

		t = new Timer();
		task = new TimerTask() {

			@Override
			public void run() {
				runOnUiThread(new Runnable() {

					@Override
					public void run() {

						tv1.setText(time + "");
						if (time < 180) {
							time += 1;
						} else {
							showAlert();
						}
					}
				});
			}
		};
		t.scheduleAtFixedRate(task, 0, 1000);

	}

	private void showFailureToast() {
		Toast toast = Toast.makeText(getApplicationContext(), "sorry,try again!!", Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
		toast.show();
	}

	private void showSuccessToast() {
		Toast toast = Toast.makeText(getApplicationContext(), "You got it", Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
		toast.show();
	}

	private void showAlert() {

		AlertDialog.Builder builder = new AlertDialog.Builder(HardGame2.this);
		builder.setTitle("PAM PAM PAMM");
		builder.setMessage("OOPS .. TIME OUT ! Wanna try again...???");
		builder.setNegativeButton("try again", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// do nothing

				Intent intent = new Intent(getApplicationContext(), HardGame2.class);
				startActivity(intent);
			}
		});
		builder.setNeutralButton("exit", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO email sending
			}
		});

		builder.show();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.hard_game2, menu);
		return true;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		// stop audio when pressed back button
		player.stop();
		player.reset();
	}

	@Override
	protected void onPause() {
		super.onPause();
		player.pause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		player.start();
	}
}
