package com.eureka.puzzlebuzzle.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class ScorePreferences {
	private static String SCORE = "score";
	private SharedPreferences preferences;

	public ScorePreferences(Context context) {
		preferences = context.getSharedPreferences("globalScore", Context.MODE_PRIVATE);
	}

	public void setScore(int score) {
		Editor editor = preferences.edit();
		editor.putInt(SCORE, score);
		editor.commit();
	}

	public int getScore() {
		return preferences.getInt(SCORE, 0);
	}
	
	public void clearScore() {
		// Sets default values
		setScore(0);
	}
}
