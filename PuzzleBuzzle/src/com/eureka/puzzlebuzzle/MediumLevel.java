/**
 * 
 */
package com.eureka.puzzlebuzzle;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;

import com.eureka.puzzlebuzzle.games.medium.MediumGame1;
import com.eureka.puzzlebuzzle.games.medium.MediumGame2;
import com.eureka.puzzlebuzzle.games.medium.MediumGame3;
import com.eureka.puzzlebuzzle.games.medium.MediumGame4;

/**
 * @author Administrator
 * 
 */
public class MediumLevel extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {

		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// Remove notification bar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.medium);

findViewById(R.id.instructionsm).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), InstructionsActivity.class);
				startActivity(intent);
			}});

		findViewById(R.id.website_linkm).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("http://www.geeklabs.co.in"));
				startActivity(intent);
			}
		});

		findViewById(R.id.medium1).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(getApplicationContext(), MediumGame1.class);
				startActivity(intent);

			}
		});

		findViewById(R.id.medium2).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), MediumGame2.class);
				startActivity(intent);
			}
		});
		findViewById(R.id.medium3).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), MediumGame3.class);
				startActivity(intent);
			}
		});
		findViewById(R.id.medium4).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), MediumGame4.class);
				startActivity(intent);
			}
		});
	}

}
