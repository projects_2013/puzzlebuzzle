package com.eureka.puzzlebuzzle;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;

import com.eureka.puzzlebuzzle.games.easy.EasyGame1;
import com.eureka.puzzlebuzzle.games.easy.EasyGame2;
import com.eureka.puzzlebuzzle.games.easy.EasyGame3;
import com.eureka.puzzlebuzzle.games.easy.EasyGame4;

public class EasyLevel extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// Remove notification bar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.easy);
findViewById(R.id.instructionse).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), InstructionsActivity.class);
				startActivity(intent);
			}});


		findViewById(R.id.website_linke).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("http://www.javatpoint.com/android-tutorial"));
				startActivity(intent);
			}
		});

		findViewById(R.id.easy1).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(getApplicationContext(), EasyGame1.class);
				startActivity(intent);

			}
		});

		findViewById(R.id.easy2).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), EasyGame2.class);
				startActivity(intent);
			}
		});
		findViewById(R.id.easy3).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), EasyGame3.class);
				startActivity(intent);
			}
		});
		findViewById(R.id.easy4).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), EasyGame4.class);
				startActivity(intent);
			}
		});
	}

}
